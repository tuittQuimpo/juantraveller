Capstone 1 requirements
- fav icon
- responsive
- no horizontal scroll
- no dead links
- all external links should open a new tab
- minimum 3 pages
- landing page
	- company name / company slogan
	- button / anchor tag to your home page
- NO DEFAULT FONTS
- no lorem ipsum
- no br
- no inline/internal styling
- all styling should be in your external css file
- do not make another bootstrap website
- no templates